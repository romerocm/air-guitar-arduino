# Guitarra de Aire con Arduino

Este repositorio contiene el código, los recursos y los materiales para el proyecto final de Electronica con Machine Learning!

A alto nivel, este proyecto se ocupa de dos modelos diferentes de Machine Learning.

Se ejecuta en el Teensy 4.1: un proyecto que clasifica los datos de la IMU gestual en dos clases
rasgueo o no rasgueo, mientras que otro modelo se ocupa de clasificar los datos de 
sensores de flexión conectados a cuatro dedos en 13 acordes diferentes.

> El repositorio está organizado de la siguiente manera:

1. La carpeta base contiene información y recursos de apoyo, incluyendo el esquema del
circuito utilizado. 

2. La carpeta de archivos de audio incluye los archivos de audio utilizados para el proyecto. En concreto, el subdirectorio chord_recordings
contiene todos los archivos de audio que se reproducen al tocar el acorde correspondiente. Los archivos 
se denominan con el nombre del acorde asociado y todos tienen formato .wav.

3. El directorio data contiene todos los datos recogidos para este proyecto, utilizados para el entrenamiento y la validación.

4. El subdirectorio flex contiene los datos de los sensores flex, mientras que el subdirectorio imu contiene los datos
de la IMU. Todos los datos se almacenan en formato .csv como datos de series temporales. Todos los datos que pertenecen a una clase
se almacenan en un archivo .csv con el nombre de la clase asociada, y los puntos de datos separados se separan
por filas vacías.

5. El directorio data_collection contiene todos los scripts utilizados para recoger los datos de ambos modelos. Hay dos 
subdirectorios, uno para cada uno de los modelos. Ambos incluyen un script python y un proyecto Platform.IO
. El script de python incluye más instrucciones en la parte superior como comentarios.

6. El directorio de despliegue contiene el código fuente final y compilado del proyecto. Es un proyecto Platform.IO
e incluye los archivos de cabecera de los modelos utilizados. La mayor parte del código fuente se incluye en el subdirectorio 
src. La carpeta también contiene el script deployment.py, que lee los datos en serie del
Teensy que ejecuta el proyecto de despliegue, y reproduce el archivo de audio deseado. Hay instrucciones más detalladas en la parte superior del archivo python.
del archivo python.

7. La carpeta experimention contiene múltiples proyectos Arduino/Platform.IO que utilizamos en nuestras pruebas para interactuar
con la placa Teensy.

8. La carpeta notebooks contiene todos los Jupyter Notebooks que utilizamos para entrenar nuestros modelos. La carpeta contiene
dos notebooks para cada tipo de sensor, uno para entrenar un modelo totalmente conectado y otro para entrenar un modelo convolucional. (https://www.ibm.com/cloud/learn/convolutional-neural-networks)

9. Por último, el directorio de resultados incluye los resultados de todos nuestros intentos de entrenamiento del modelo para ambos sensores, incluyendo 
las matrices, así como los archivos TFLite y los archivos del modelo.

## Gestos de acordes

Este proyecto soporta 13 acordes. Son los siguientes:
- (A) LA , LA Menor 
- (B) SI, SI Menor 
- (C) DO, DO Menor 
- (D) RE, RE Menor 
- (E) MI, MI Menor 
- (F) FA, FA menor 
- (G) SOL, SOL menor 

Dado que es difícil utilizar las posiciones reales de las manos de estos acordes, tanto físicamente como para la recogida y clasificación de datos, hemos 
hemos creado nuestros gestos para representar estos acordes. Cada gesto se compone de una combinación de 4 dedos: índice, medio, anular 
y meñique, en ese orden. Un 1 significa que el dedo se mantiene recto, mientras que un 0 significa que el dedo está doblado hacia la palma de la mano.
Utilizando esta codificación, los siguientes son los gestos para cada uno de los Acordes soportados: 

a: 0001

am: 1001

b: 1000

bm: 1111

c: 0110

cm: 1011

d: 0010

dm: 0100

e: 0000

em: 1100

f: 1101

g: 1101

gm: 1010

## Diseño y configuración del hardware

El aspecto del hardware del proyecto es un componente central del sistema global. A continuación se muestra el diagrama del circuito de los componentes y sus respectivas conexiones. 

<img src="https://i.imgur.com/byG87OU.png" width=250 width=300 align=center>

Este proyecto utiliza sensores conectados a los guantes para recoger los datos y ejecutar la aplicación. En uno de los guantes se acopla la IMU. Esto es para detectar el rasgueo. El otro guante tiene 4 sensores flexibles conectados, uno para cada dedo excepto el pulgar, y esto se utiliza para identificar los acordes. Los guantes tienen cables que salen de sus pines y se conectan a la placa principal, que contiene los componentes adicionales.

<img src="https://i.imgur.com/BqIihAz.jpg" width=500 align=left>
<img src="https://i.imgur.com/P1VEzD1.jpg" width=250 align=left>
<img src="https://i.imgur.com/SSiuyKf.jpg" width=250 align=left>
<img src="https://i.imgur.com/XhTgEQO.jpg" width=250 align=left>
